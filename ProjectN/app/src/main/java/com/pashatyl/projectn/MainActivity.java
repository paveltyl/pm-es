package com.pashatyl.projectn;

import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.*;

import java.util.Locale;

public class MainActivity extends Activity implements CompoundButton.OnCheckedChangeListener {
    TextView tvInfo;
    EditText etInput;
    Button bControl;
    TextView tvTip;
    int guess;
    int prevValue = 0;
    int correction;
    boolean gameFinished;
    boolean tipsEnabled = false;
    String inputText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvInfo = (TextView) findViewById(R.id.InfoLabel);
        etInput = (EditText) findViewById(R.id.enterField);
        bControl = (Button) findViewById(R.id.enterButton);
        tvTip = (TextView) findViewById(R.id.tvTip);
        tvTip.setVisibility(View.INVISIBLE);
        tvTip.setText(R.string.firstTip);
        guess = (int) (Math.random() * 100);
        gameFinished = false;
        ((Switch) findViewById(R.id.swHelp)).setOnCheckedChangeListener(this);
    }

    public void onClick(View v) {
        if (!gameFinished) {
            inputText = etInput.getText().toString();
            if (inputText.isEmpty()) {
                return;
            }
            int inp = Integer.parseInt(inputText);
            if (inp > 100) {
                Toast.makeText(getApplicationContext(), R.string.incorrectValue, Toast.LENGTH_LONG).show();
                etInput.setText("");
                return;
            }
            correction = Math.abs(prevValue - inp) / 2;
            if (correction == 0) {
                correction = 1;
            }
            if (inp > guess) {
                tvInfo.setText(getResources().getString(R.string.ahead));
                correction *= -1;
            }
            if (inp < guess) {
                tvInfo.setText(getResources().getString(R.string.behind));
            }
            if (inp == guess) {
                tvInfo.setText(getResources().getString(R.string.hit));
                bControl.setText(getResources().getString(R.string.playMore));
                gameFinished = true;
                tvTip.setText("");
            } else {
                prevValue = inp;
                tvTip.setText(String.format("%s %d", R.string.tryToCheck, inp + correction));
            }
        } else {
            prevValue = 0;
            guess = (int) (Math.random() * 100);
            bControl.setText(getResources().getString(R.string.inputValue));
            tvInfo.setText(getResources().getString(R.string.tryToGuess));
            gameFinished = false;
            tvTip.setText(R.string.firstTip);
        }
        etInput.setText("");
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        tvTip.setVisibility(isChecked ? View.VISIBLE : View.INVISIBLE);
    }
}